export default function createWidget() {
  const widget = document.createElement('div');
  const styles = document.createElement('style');

  widget.className = 'widget';
  styles.className = 'for-widget';
  widget.innerHTML = 
  `
  <div class="drag">
  <h2 class="widget-title">Search node element</h2>
  <span id="close-btn" class="close"></span>
  </div>
  <input class="search-input" type="text" name="search" placeholder="Key in a CSS selector to find">
  <button id="search-btn" class="w-btn w-search-btn">Search</button>
  <ul class="w-btn-list">
    <li><button id="prev-btn" class="w-btn prev-btn" disabled>Prev</button></li>
    <li><button id="next-btn" class="w-btn next-btn" disabled>Next</button></li>
    <li><button id="parent-btn" class="w-btn parent-btn" disabled>Parent</button></li>
    <li><button id="child-btn" class="w-btn child-btn" disabled>Child</button></li>
  </ul>
  `;
  styles.innerText = `
  * {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
  }

  h2{
    margin: 0;
  }

  .drag {
    cursor: move;
  }

  .widget {
    position: absolute;
    width: 300px;
    border: 3px solid #1181FB;
    padding: 5px;
    top: 0;
    left: calc(100% - 300px);
    z-index: 1000;
    background-color: #F2F6F9;
    font-family: Arial;
    color:#374C70;
  }

  .widget-title {
    padding: 15px 0;
    user-select: none;
    font-size: 24px;
    font-weight: 500;
    line-height: 1.1;
  }

  .w-btn-list {
    list-style: none;
    display:flex;
    justify-content: space-between;
    padding: 15px 0;
    font-size: 13px;
  }

  .search-input {
    width: 70%;
  }
  
  .w-btn {
    width: 70px;
    border-radius: 5px;
    cursor: pointer;
    background-color: white;
  }

  button:disabled {
    color: graytext;
    cursor: default;
  }
  
  .w-search-btn {
    float: right;
  }

  .highlight {
    border: 1px solid red;
  }
  
  .close {
    position: absolute;
    top: 0;
    left: calc(100% - 16px);
    width: 16px;
    height: 16px;
    opacity: 0.3;
    cursor: pointer;
  }

  .close:hover {
    opacity: 1;
  }

  .close:before, .close:after {
    position: absolute;
    left: 6px;
    content: ' ';
    height: 16px;
    width: 3px;
    background-color: black;
  }

  .close:before {
    transform: rotate(45deg);
  }

  .close:after {
    transform: rotate(-45deg);
  }
  `;
  return [widget, styles]
};
