import createWidget from './createWidget.js';

(function () {
  /* load */
  if(document.body.querySelector(".widget")){
    return;
  } else if(document.readyState === 'complete') {
    appendToDOM(widget, styles);
  } else {
    document.onreadystatechange = function() {
      if(document.readyState === 'complete') {
        appendToDOM(widget, styles);
      };
    };
  };
  
  const [widget, styles] = createWidget();

  widget.addEventListener('click', mainCallback);
    
  function appendToDOM(widget, styles) {
    document.body.appendChild(widget);
    document.head.appendChild(styles);
  }
  
  let selectedItem;

  function mainCallback(event) {
    let target = event.target;

    if(target.tagName !== "BUTTON" && target.tagName !== "SPAN") return;

    switch(target.id) {
      case "close-btn": closeWidget();
        break;
      case "search-btn": searchHandler();
        break;
      case "prev-btn": btnHandler(selectedItem, 'previousElementSibling');
        break;
      case "next-btn": btnHandler(selectedItem, 'nextElementSibling');
        break;
      case "parent-btn": btnHandler(selectedItem, 'parentElement');
        break;
      case "child-btn": btnHandler(selectedItem, 'firstElementChild');
        break;
    };

    changeBtnStatus();
  };

  function highLight(item) {
    if(selectedItem) {
      selectedItem.classList.toggle("highlight");
    };
    selectedItem = item;
    selectedItem.classList.add("highlight");
  };

  function closeWidget() {
    if(selectedItem) {
      selectedItem.classList.remove("highlight");
    };
    widget.remove();
    styles.remove();
  };

  function searchHandler() {
    const inputValue = document.querySelector('.search-input').value;
    let item = document.querySelector(inputValue);
    highLight(item);
  };

  function btnHandler(item, modifier) {
    highLight(item[modifier]);
  }

  function changeBtnStatus() {
    let btns = document.querySelectorAll(".w-btn");
    console.log(btns)
    Array.from(btns).map((el) => el.disabled = !checkBtnWithElem(el, selectedItem));
  };

  function checkBtnWithElem(button, element) {
    let isEnabled = true;
    switch(button.id) {
      case "prev-btn": isEnabled = Boolean(element.previousElementSibling);
        break;
      case "next-btn": isEnabled = Boolean(element.nextElementSibling);
        break;
      case "parent-btn": isEnabled = Boolean(element.parentElement);
        break;
      case "child-btn": isEnabled = Boolean(element.firstElementChild);
        break;
    };
    return isEnabled;
  };

  /* Make widget draggable */
  const drag = widget.querySelector(".drag");
  drag.addEventListener('mousedown', dragging);

  function dragging(event) {
    
    let target = document.querySelector(".widget");
    let shiftX = event.clientX - target.getBoundingClientRect().left;
    let shiftY = event.clientY - target.getBoundingClientRect().top;

    function moveAtX(pageX) {
      target.style.left = pageX - shiftX + 'px';
    }

    function moveAtY(pageY) {
      target.style.top = pageY - shiftY + 'px';
    }
  
    function onMouseMoveX(event) {
      if (event.clientX <= shiftX) {
        return;
      }
      if (event.clientX >= window.innerWidth - (target.getBoundingClientRect().width - shiftX)) {
        return;
      }
      moveAtX(event.pageX);
    }

    function onMouseMoveY(event) {
      if (event.clientY <= shiftY) {
        return;
      }
      if (event.clientY >= window.innerHeight - (target.getBoundingClientRect().height - shiftY)) {
        return;
      }
      moveAtY(event.pageY);
    }
  
    document.addEventListener('mousemove', onMouseMoveX);
    document.addEventListener('mousemove', onMouseMoveY);

    document.addEventListener("mouseup", function() {
      document.removeEventListener('mousemove', onMouseMoveX);
      document.removeEventListener('mousemove', onMouseMoveY);
    })
  }
})()